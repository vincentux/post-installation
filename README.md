Bonjour,

En partant d'une base Debian-Minimal ou Ubuntu-Server, ce dépôt a pour objectif d'automatiser la post-installation d'une couche graphique avec tous les paquets nécessaires pour obtenir, au final, un système d'exploitation fonctionnel.

# Prérequis

- Créez une clé USB bootable contenant l'image iso de [**debian-9.5.0-i386-netinst.iso**](https://www.debian.org/CD/netinst/) ou d'[**ubuntu-16.04.5-server-i386.iso**](http://releases.ubuntu.com/16.04/ubuntu-16.04.5-server-i386.iso).
- Assurez vous de disposer d'une connexion internet filaire.
- Installez Debian-Minimal ou Ubuntu-Server sur votre ordinateur. Durant l'installation, vous allez vous retrouver devant cet écran :

    ![2.png](2.png)

- S'il y a des astérisques entre les crochets, supprimez ces derniers en utilisant la touche "espace".
- Lorsque l'installation sera terminée, vous allez vous retrouver devant une simple console tty qui va vous demander votre identifiant et votre mot de passe. Obéissez
- Ensuite, pour **Debian-Minimal**, prière d'entrer cette commande qui permet de mettre à jour les paquets et de compresser/décompresser un dossier :  
    **`su -l root -c "apt update && apt install zip"`**
- Renseignez le mot de passe du compte root.
- Contrairement à **su** sans argument, cette commande ne vous laisse pas en **« root »**. Lorsqu’elle est exécutée, vous redevenez utilisateur. C’est très important! 

- Pour **Ubuntu-Server**, la commande est :
      **`sudo apt update && sudo apt install wget zip`**
- **Sudo** est installé nativement sur Ubuntu. Par contre, il n’y a pas de compte **root**. C’est la politique d’Ubuntu. Si vous tenez absolument à en créer un, il suffit d’entrer cette commande dans un terminal : **`sudo passwd root`**

- Entrez cette commande qui va télécharger un dossier compressé de mon dépôt git:  
    **`wget --no-check-certificate https://framagit.org/vincentux/post-installation/-/archive/master/post-installation-master.zip`**
- Décompressez le dossier et déplacez les fichiers dans votre répertoire courant grâce à cette commande double :  
    **`unzip post-installation-master.zip && mv post-installation-master/* ~`**
- Prenez garde de ne pas omettre l'astérisque et le tilde! L'astérisque copie le contenu du dossier **post-installation-master** dans votre répertoire utilisateur représenté ici par le tilde (**~**).

# Post-installation

- Pour Debian-Minimal, entrez dans votre console, la double commande ci-après : **`chmod +x ./post_install.sh && ./post_install.sh`**
- Pour Ubuntu-Server, entrez dans votre console, la double commande ci-après : **`chmod +x ./post_install_ubserv.sh && ./post_install_ubserv.sh`**
- Elle confère les droits d’exécution au fichier **`post_install.sh`** ou **`post_install_ubserv.sh`** avant de lancer ce dernier.
- Le script est lancé. La procédure de post-installation démarre... Il ne vous reste plus qu'à suivre scrupuleusement les instructions.

# To-do list

- Commenter le code. **(Edit du 27.10.2018 : Fait)**
- Simplifier le code. **(Edit du 27.10.2018 : Fait)**
- Rajouter une fenêtre de déconnexion.
- Compléter les scripts avec de nouveaux paquets.
- Tester les scripts avec d’autres installations minimales telles qu'Antix ou une image iso destinée à Raspberry.
- Remplacer nitrogen par feh qui offre des possibilités de configuration beaucoup plus intéressantes. **(Edit du 27.10.2018 : Fait)**
- Rajouter une calculatrice.
