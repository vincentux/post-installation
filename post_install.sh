#!/bin/bash

# Copyright © 2018 Benoît Boudaud <https://miamondo.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

user=$USER

# Suppression des fichiers inutiles :
rm ~/post-installation-master.zip
rm -r ~/post-installation-master
rm ~/README.md
rm ~/2.png
rm ~/post_install_ubserv.sh

echo "Bonjour $user. L'ordinateur va débuter la post-installation \
dès que vous aurez renseigné le mot de passe du compte ROOT."

# Installation de dialog, sudo et ajout de l'utilisateur au groupe sudo :
su root -c "apt install dialog sudo \
&& adduser $user sudo \
&& echo '$user   ALL=(ALL:ALL) ALL' > visudo"

dialog --title "Programme de post-installation" --msgbox \
"Accrochez-vous moussaillons! Le manège va partir!" 5 60

# Installation des programmes choisis par l'utilisateur.
# Ils sont classés par ordre alphabétique :
sudo apt update \
&& sudo apt install \
aptitude \
compton \
evince \
feh \
firefox-esr \
gnome-disk-utility \
gnome-icon-theme \
gparted alsa-utils \
guvcview \
libglib2.0-bin \
libreoffice \
libreoffice-l10n-fr \
lightdm \
lightdm-gtk-greeter \
lxterminal \
man-db \
obconf \
obmenu \
openbox \
openssh-server \
pavucontrol \
plank \
pluma \
python3 \
python-pil.imagetk \
python3-tk \
redshift \
shutter \
simple-scan \
synaptic \
thunar \
tint2 \
network-manager-gnome \
xdg-utils \
xinit \
xserver-xorg
 
sudo apt autoremove # Nettoyage

# Création des répertoires de configuration pour openbox, tint2 et plank :
mkdir -p ~/.config/openbox
mkdir -p ~/.config/tint2
mkdir -p ~/.config/plank/dock1/launchers
touch ~/.config/tint2/tint2rc # Création du fichier tint2rc

# Création de l'icône applications dans le dock plank :
{ 
echo "[PlankDockItemPreferences]"
echo "Launcher=file:///usr/share/applications" 
} > ~/.config/plank/dock1/launchers/applications.dockitem
  
# Configuration de plank
gsettings set net.launchpad.plank.dock.settings:/net/launchpad/plank/docks/dock1/ position bottom
gsettings set net.launchpad.plank.dock.settings:/net/launchpad/plank/docks/dock1/ theme Transparent

# Configuration de tint2 :
sudo sed -i "s/bottom center horizontal/top center horizontal/g" /etc/xdg/tint2/tint2rc
cat /etc/xdg/tint2/tint2rc > ~/.config/tint2/tint2rc

# Création d'un sous-répertoire wallpapers pour y stocker le fond d'écran :
mkdir -p ~/Images/Wallpapers
mv ~/fedora_wallpaper.jpg ~/Images/Wallpapers

# Choix des programmes lancés au démarrage d'Openbox :
{
echo "plank &"
echo "compton &"
echo "feh --bg-scale ~/Images/Wallpapers/fedora_wallpaper.jpg &" 
echo "tint2 &"
echo "redshift &"
} > ~/.config/openbox/autostart # Écriture dans le fichier texte   

dialog --title "Programme de post-installation" --msgbox \
"Ouf! Installation terminée!" 5 40

# Destruction du fichier d'installation
rm ~/post_install.sh

sudo reboot

